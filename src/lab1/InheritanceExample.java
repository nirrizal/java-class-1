package lab1;

public class InheritanceExample {
    public static void main(String[] args) {
//        lab1.Teacher teacher = new lab1.Teacher();
        ComputerTeacher computerTecher = new ComputerTeacher();
        JavaTeacher javaTeacher = new JavaTeacher();
    }
}

class Teacher {
    static int salary = 10000;
    Teacher() {
        System.out.println("Salary of teacher is : " + salary);
    }
}

class ComputerTeacher extends Teacher{
    ComputerTeacher(){
        System.out.println("The salary of computer teacher is : " + salary * 2);
    }
}

class JavaTeacher extends Teacher{
    JavaTeacher(){
        System.out.println("Salary of java teacher is : " + salary * 3);
    }
}




