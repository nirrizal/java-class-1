package lab1;

public class MultipleInheritance implements Interface1, Interface2 {

    public void m1(){
        System.out.println("Value of i in Interface 1 : " + Interface1.i);
        System.out.println("Value of i in Interface 2 : " + Interface2.i);
    }

    public static void main(String[] args) {
        MultipleInheritance multipleInheritance = new MultipleInheritance();
        multipleInheritance.m1();
    }
}
