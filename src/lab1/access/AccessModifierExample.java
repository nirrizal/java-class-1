package lab1.access;

import lab1.inheritance.AccessModifier;

public class AccessModifierExample {

    public static void main(String[] args) {

        AccessModifier accessModifier = new AccessModifier();

        accessModifier.addVariables();

        accessModifier.d = 25;
        accessModifier.setA(100);

        accessModifier.addVariables();
    }

}
