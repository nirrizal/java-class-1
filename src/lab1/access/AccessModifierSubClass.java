package lab1.access;

import lab1.inheritance.AccessModifier;

public class AccessModifierSubClass extends AccessModifier {

    public void show(){
        System.out.println("The value of c is : "+ c);
    }

    public static void main(String[] args) {
        System.out.println("The value of c is : " + c);
    }

}
