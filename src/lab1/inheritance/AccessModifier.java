package lab1.inheritance;

public class AccessModifier {

    public int d = 40;
    static protected int c = 30;
    private int a = 10;
    private int b = 20;

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public void addVariables() {
        System.out.println("Sum : " + (a + b + c + d));
    }

}
