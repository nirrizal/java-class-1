package lab2.thread;

public class ThreadClass {

    public static void main(String[] args) {
        MyClass myClass = new MyClass();
        myClass.start();
    }

}

class MyClass extends Thread{
    public void run(){
        System.out.println("My class is running");
    }
}
