package lab2.thread;

public class RunnableInterface {

    public static void main(String[] args) {
        Thread t1 = new Thread(new MyClass1());
        t1.start();
    }

}

class MyClass1 implements Runnable{
    public void run(){
        System.out.println("MyClass1 running");
    }
}
