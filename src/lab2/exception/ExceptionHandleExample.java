package lab2.exception;

import java.io.FileReader;
import java.io.IOException;

public class ExceptionHandleExample {

    static int a = 10;
    static int b = 0;

    public static void main(String[] args) {
        try{
            FileReader f = new FileReader("foo.txt");
            System.out.println(a/b);
        } catch (IOException e){
            System.out.println("IO Exception");
        } catch (ArithmeticException ae){
            System.out.println("Arithmetic exception");
        } finally {
            System.out.println("Finally is called");
        }
    }

}
