package lab2.exception;

public class ThrowException {
    public static void main(String[] args) {
        try{
            throw new MyException(2);
        } catch (MyException e){
            System.out.println(e);
        }
    }
}

class MyException extends Exception{
    int e;
    MyException(int b){
        e = b;
    }
    public String toString(){
        return ("Exception Number = " + e);
    }
}
